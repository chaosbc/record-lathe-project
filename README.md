# Records Engraving Machine
 This project is about creating and programming a vinyl Records engraving machine
 This is a work in progress and more documentation (3D models, diagrams...) will be progressively uploaded in this repository

The control panel is based on ATMega328p arduino microcontrollers along with L298D dual H-Bridge Motor Driver.
 It can listen to any MIDI Devices (Eg: a computer with a Digital Audio Workstation such as Logic or Pro-Tools)
 It can also be manually operated using buttons
 Thanks to a dedicated Adafruit library, A small OLED screen displays all actions ordered to the machine
 Therefore, the mentioned library must be locally present during the compilation and the firmware flashing.

 Have fun
 
 2017 Benjamin Carette

