// Record lathe v10 *****bug avec stopit corrigé + variable pitch tentative
//
// variables pins
int dir1PinA = 9;
int dir2PinA = 3;
int motorpin=11;

//variables vitesses moteur

int playvalue=75;
int echovalue=100;
int timevalue=150;
int fastvalue=255;
int stopvalue=0;
int pinsens2=13;
int statedir1PinA=LOW;
int statedir2PinA=HIGH;
int pinsens=6;
// variables boutons poussoirs

int playbtn=2;
int echobtn=4;
int timebtn=7;
int stopbtn=A0;
int fastbtn=8;
int interval=5;

// vitesse initiale moteur

int level=0;
int prevlevel=0;

// delay debounce
int delai=10;

// ralentissement pwm
int steps=10;


#include <SPI.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
byte commandByte;
byte noteByte;
byte velocityByte;
byte noteOn = 144;
#define OLED_RESET 4
Adafruit_SSD1306 display(OLED_RESET);



void setup()   {              
  Serial.begin(31250);
  display.begin(SSD1306_SWITCHCAPVCC, 0x3C);
display.clearDisplay();
 
  pinMode (motorpin,OUTPUT);
  pinMode (playbtn, INPUT);
  pinMode (echobtn, INPUT);
  pinMode (stopbtn, INPUT);
  pinMode (fastbtn, INPUT);
  pinMode (timebtn, INPUT);
  pinMode (pinsens, INPUT);
  pinMode (pinsens2, INPUT);
  pinMode (statedir1PinA, OUTPUT);
  pinMode (statedir2PinA, OUTPUT);
}
void checkMIDI(){
  do{
    if (Serial.available()){
      commandByte = Serial.read();//read first byte
      noteByte = Serial.read();//read next byte
      velocityByte = Serial.read();//read final byte
      if (commandByte == noteOn){//if note on message
    if (noteByte == 60 && velocityByte > 0 && level<playvalue){playinc();}
   if (noteByte ==60 && velocityByte > 0 && level>playvalue){playdec();}      
   if (noteByte ==62 && velocityByte > 0  && prevlevel<echovalue){echoinc();}
   if (noteByte ==62 && velocityByte > 0  && level>echovalue){echodec();}
   if (noteByte ==64 && velocityByte > 0  && prevlevel<timevalue){timeinc();}
   if (noteByte ==64 && velocityByte > 0  && level>timevalue){timedec();}
   if (noteByte ==65 && velocityByte > 0  && prevlevel<fastvalue){fastinc();}
   if (noteByte ==65 && velocityByte > 0 && level>fastvalue){fastdec();}
   if (noteByte ==72 && velocityByte > 0  && prevlevel>0){stopit();}


  }}}
 
 
 
  while (Serial.available() > 2);//when at least three bytes available
}


void stopit(){
   display.clearDisplay();
    display.setTextSize(2);
    display.setTextColor(WHITE);
    display.setCursor(25,25);
    display.println("Stop");
    display.display();
    for (int i=prevlevel; i> 0; i--){
    analogWrite(motorpin, i);delay (steps);}level=stopvalue;prevlevel=level; delay(delai);
  }  
      
void playinc(){
     display.clearDisplay();
    display.setTextSize(2);
    display.setTextColor(WHITE);
    display.setCursor(25,25);
    display.println("playinc");
    display.display();
    for (int i=prevlevel; i <= playvalue; i++){
      int pitch=analogRead(A1);
  pitch=map(pitch, 0, 1023, 0, 255);
    analogWrite(motorpin, i); delay (steps);}level=playvalue;prevlevel=level; delay(delai);}

 
void playdec(){

  display.clearDisplay();
  display.setTextSize(2);
  display.setTextColor(WHITE);
  display.setCursor(25,25);
  display.println("playdec"); 
  display.display();
for (int i=prevlevel; i> playvalue; i--){
int pitch=analogRead(A1);
  pitch=map(pitch, 0, 1023, 0, 255);
      analogWrite(motorpin, i);delay (steps);}level=playvalue;prevlevel=level; delay(delai);}

void echoinc(){
  
    display.clearDisplay();
    display.setTextSize(2);
    display.setTextColor(WHITE);
    display.setCursor(25,25);
    display.println("Echoinc");
    display.display();
    for (int i=prevlevel; i <= echovalue; i++){
    analogWrite(motorpin, i);delay (steps);}level=echovalue;prevlevel=level; delay(delai);}

void echodec(){
  
    display.clearDisplay();
    display.setTextSize(2);
    display.setTextColor(WHITE);
    display.setCursor(25,25);
    display.println("Echodec");
    display.display();
    for (int i=prevlevel; i> echovalue; i--){
      analogWrite(motorpin, i);delay (steps);}level=echovalue;prevlevel=level; delay(delai);}
        
 void timeinc(){
    display.clearDisplay();
    display.setTextSize(2);
    display.setTextColor(WHITE);
    display.setCursor(25,25);
    display.println("Timeinc");
    display.display();
     for (int i=prevlevel; i <= timevalue; i++){

      analogWrite(motorpin, i);delay (steps);}level=timevalue;prevlevel=level; delay(delai);
 } 

void timedec(){
  display.clearDisplay();
    display.setTextSize(2);
    display.setTextColor(WHITE);
    display.setCursor(25,25);
    display.println("Timedec");
    display.display();
     for (int i=prevlevel; i> timevalue; i--){
      analogWrite(motorpin, i);delay (steps);}level=timevalue;prevlevel=level; delay(delai);
    }
    
void fastinc(){
   display.clearDisplay();
    display.setTextSize(2);
    display.setTextColor(WHITE);
    display.setCursor(25,25);
    display.println("fastinc");
    display.display();
    for (int i=prevlevel; i <= fastvalue; i++){
      analogWrite(motorpin, i);delay (steps);}level=fastvalue;prevlevel=level; delay(delai);
}  
 
void fastdec(){
   display.clearDisplay();
    display.setTextSize(2);
    display.setTextColor(WHITE);
    display.setCursor(25,25);
    display.println("fastdec");
    display.display();  
    for (int i=prevlevel; i> fastvalue; i--){
      analogWrite(motorpin, i);delay (steps);}level=fastvalue;prevlevel=level; delay(delai);
}  
  

      
void loop() {
  checkMIDI();
  display.setTextSize(1);
  display.setTextColor(WHITE);
  display.setCursor(25,2);
  display.println("Chaos-Lathe");
  display.display();
   analogWrite(motorpin, level);
 digitalWrite (dir1PinA,statedir1PinA);
 digitalWrite (dir2PinA,statedir2PinA);
 // bouton sens pressé et sens initial clockwise
if (digitalRead(pinsens) == HIGH && statedir1PinA == LOW && statedir2PinA == HIGH)
{statedir1PinA=HIGH;
statedir2PinA=LOW;
}

// bouton sens2 pressé et sens initial clockwise
if (digitalRead(pinsens2) == HIGH && statedir1PinA == HIGH && statedir2PinA == LOW)
{statedir1PinA=LOW;
statedir2PinA=HIGH;}

 
 
  // le bouton play est pressé (increase)

    if (digitalRead(playbtn) == HIGH && level<playvalue){
      display.clearDisplay();
    display.setTextSize(2);
    display.setTextColor(WHITE);
    display.setCursor(25,25);
    display.println("playinc");
    display.display();
    for (int i=prevlevel; i <= playvalue; i++){

      analogWrite(motorpin, i); delay (steps);}level=playvalue;prevlevel=level; delay(delai);}
     
 
     
 // le bouton play est pressé (decrease)
 
 if (digitalRead(playbtn) == HIGH && level>playvalue){
   display.clearDisplay();
    display.setTextSize(2);
    display.setTextColor(WHITE);
    display.setCursor(25,25);
    display.println("Playdec");
    display.display();

    for (int i=prevlevel; i> playvalue; i--){

      analogWrite(motorpin, i);delay (steps);}level=playvalue;prevlevel=level; delay(delai);}
     


 // le bouton stop est pressé ?

if (digitalRead(stopbtn) == HIGH && prevlevel>0){
  display.clearDisplay();
    display.setTextSize(2);
    display.setTextColor(WHITE);
    display.setCursor(25,25);
    display.println("Stop");
    display.display();

    for (int i=prevlevel; i> stopvalue; i--){

      analogWrite(motorpin, i);delay (steps);}level=stopvalue;prevlevel=level; delay(delai);}



  // le bouton echo est pressé (increase)

if (digitalRead(echobtn) == HIGH && prevlevel<echovalue){
  display.clearDisplay();
    display.setTextSize(2);
    display.setTextColor(WHITE);
    display.setCursor(25,25);
    display.println("Echoinc");
    display.display();

    for (int i=prevlevel; i <= echovalue; i++){

      analogWrite(motorpin, i);delay (steps);}level=echovalue;prevlevel=level; delay(delai);}
     

     
  // le bouton echo est pressé (decrease)
 
 if (digitalRead(echobtn) == HIGH && level>echovalue){
   display.clearDisplay();
    display.setTextSize(2);
    display.setTextColor(WHITE);
    display.setCursor(25,25);
    display.println("Echodec");
    display.display();

    for (int i=prevlevel; i> echovalue; i--){

      analogWrite(motorpin, i);delay (steps);}level=echovalue;prevlevel=level; delay(delai);}
 

 

 // le bouton time est pressé (increase)

if (digitalRead(timebtn) == HIGH && prevlevel<timevalue){
  display.clearDisplay();
    display.setTextSize(2);
    display.setTextColor(WHITE);
    display.setCursor(25,25);
    display.println("Timeinc");
    display.display();

    for (int i=prevlevel; i <= timevalue; i++){

      analogWrite(motorpin, i);delay (steps);}level=timevalue;prevlevel=level; delay(delai);}  

     
     
     
   // le bouton time est pressé (decrease)
 
 if (digitalRead(timebtn) == HIGH && level>timevalue){
   display.clearDisplay();
    display.setTextSize(2);
    display.setTextColor(WHITE);
    display.setCursor(25,25);
    display.println("Timedec");
    display.display();

    for (int i=prevlevel; i> timevalue; i--){

      analogWrite(motorpin, i);delay (steps);}level=timevalue;prevlevel=level; delay(delai);}

 
     
     

      // le bouton fast est pressé (increase)

if (digitalRead(fastbtn) == HIGH  && prevlevel<fastvalue){
  display.clearDisplay();
    display.setTextSize(2);
    display.setTextColor(WHITE);
    display.setCursor(25,25);
    display.println("fastinc");
    display.display();

    for (int i=prevlevel; i <= fastvalue; i++){

      analogWrite(motorpin, i);delay (steps);}level=fastvalue;prevlevel=level; delay(delai);}

     
   // le bouton fast est pressé (decrease)
  
 if (digitalRead(fastbtn) == HIGH && level>fastvalue){
   display.clearDisplay();
    display.setTextSize(2);
    display.setTextColor(WHITE);
    display.setCursor(25,25);
    display.println("Fastdec");
    display.display();

    for (int i=prevlevel; i> fastvalue; i--){

      analogWrite(motorpin, i);delay (steps);}level=fastvalue;prevlevel=level; delay(delai);}

 

    } 
